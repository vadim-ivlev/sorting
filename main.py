import random
import time


def bsort(a=[]):
    buckets = [0] * 30000

    for v in a:
        buckets[v] += 1

    i = 0
    for j, v in enumerate(buckets):
        for k in range(v):
            a[i] = j
            i += 1
    # print(buckets[:5])


def main():
    a = random.choices(range(30000), k=100000)
    t0 = time.time()

    bsort(a)

    print(f"time= {(time.time() - t0)*1000:3.2f} ms")
    print(*a[:15], "...", *a[-10:])

main()