package main

import (
	"crypto/rand"
	"fmt"
	"math/big"
	"os"
	// "math/rand"
)

func bsort(a *[100000]int) {
	var buckets [30000]int
	for _, v := range a {
		buckets[v]++
	}
	i := 0
	for j, v := range buckets {
		for k := 0; k < v; k++ {
			a[i] = j
			i++
		}
	}
	fmt.Println(buckets[:5])
}

// func testBsort() {
// 	var a [100000]int
// 	for i := range a {
// 		a[i] = rand.Intn(30000)
// 	}
// 	t0 := time.Now()
// 	bsort(&a)
// 	dt := time.Now().Sub(t0)
// 	fmt.Println(dt)
// 	fmt.Println(a[:20], "...", a[99980:])
// }

func main() {
	// testBsort()

	//Max random value, a 130-bits integer, i.e 2^130 - 1
	max := new(big.Int)
	max.Exp(big.NewInt(10), big.NewInt(80), nil).Sub(max, big.NewInt(1))
	fmt.Printf("%80d\n", max)

	s := ""
	for i := 0; i < 100000; i++ {
		//Generate cryptographically strong pseudo-random between 0 - max
		n, err := rand.Int(rand.Reader, max)
		if err != nil {
			//error handling
		}

		//String representation of n in base 32
		// nonce := n.Text(10)

		s += fmt.Sprintf("%80d\n", n)
	}

	f, _ := os.Create("numbers.txt")
	defer f.Close()
	f.WriteString(s)

}
